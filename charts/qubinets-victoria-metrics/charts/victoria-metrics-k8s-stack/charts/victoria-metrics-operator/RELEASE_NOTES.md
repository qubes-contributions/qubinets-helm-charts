# Release notes for version 0.28.1

**Release date:** 2024-02-21

![AppVersion: 0.41.2](https://img.shields.io/static/v1?label=AppVersion&message=0.41.2&color=success&logo=)
![Helm: v3](https://img.shields.io/static/v1?label=Helm&message=v3&color=informational&logo=helm)

- bump version of VM operator to [0.41.2](https://github.com/VictoriaMetrics/operator/releases/tag/v0.41.2)

